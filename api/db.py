from pymongo import MongoClient

from .cfg_manager import config as CONFIG

instance = None

class DB(object):
    """
    Class to manage connection with MongoDb
    """
    def __init__(self):
        configuration = CONFIG('mongo')
        self.host = configuration.get('mongo_host_name')
        print(self.host)
        self.port = int(configuration.get('mongo_port',27017))
        self.db_name = configuration.get('mongo_db_name')

        self.conn = None

    def __call__(self):
        """
        Returns active connection
        """
        if self.conn != None:
            return self.conn
        self.mongo_client = MongoClient(self.host, self.port)
        self.conn = self.mongo_client[self.db_name]
        return self.conn

def db_conn():
    global instance
    if instance != None:
        return instance()
    instance = DB()
    return instance()
