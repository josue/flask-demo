import os.path
import configparser

CONFIG = None

class ConfigManager(object):
    """
    ConfigManager class to read .ini stype config files 
    """
    def __init__(self, _file):
        self.configuration = configparser.ConfigParser()
        self.configuration.read(_file)

    def __call__(self, section):
        return self._get_config_section(section)

    def _get_config_section(self, section):
        """
        Searchs over the configuration object and returns a dictionary
        with the configuration values for the specified section
        """
        config_dict = {}
        options = self.configuration.options(section)
        for option in options:
            config_dict[option] = self.configuration.get(section, option)
        return config_dict

def load_cfg():
    """
    Loads default configuration file
    """
    config_path = './configuration.cfg'
    if not os.path.isfile(config_path):
        config_path = '/etc/api/configuration.ini'
    return ConfigManager(config_path)

    
def init():
    """
    Returns current config loaded instance
    """
    global CONFIG
    if CONFIG is not None:
        return CONFIG
    CONFIG = load_cfg()
    return CONFIG

config = init()

__all__ = [ 'config' ]
