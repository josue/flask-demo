
class EarthquakeErr(Exception):
    pass


class Earthquake(object):

    def __init__(self, db_conn):
        self.db_conn = db_conn

    def search(self, terms=None):
        """
        Makes query to DB using terms as parameter
        terms must be a dictionary
        """
        if terms == None:
            terms = {}
        for eq in self.db_conn['earthquakes'].find(terms):
            del eq['_id']
            yield eq

    @staticmethod
    def earthquake_validator(earthquake):
        """
        Checks if earthquake data has the min required fields
        """
        mini_reqs = ['time', 'latitude', 'longitude', 'id', 'type', 'locationSource']
        keys = earthquake.keys()
        for m in mini_reqs:
            if m not in keys:
                raise EarthquakeErr('{} not in required fields'.format(m))
        return True

    def create(self, earthquake):
        """
        Creates new earthquake in DB
        """
        Earthquake.earthquake_validator(earthquake)
        result = self.db_conn['earthquakes'].insert_one(earthquake)
        return result
