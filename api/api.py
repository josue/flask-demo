import json

from flask import Flask, request

from .earthquakes import Earthquake
from .db import db_conn


app = Flask(__name__)
#app.config.from_envvar('EARTHQUAKE_SETTINGS')

@app.route('/earthquake/<key>/<value>')
def get_earthquake(key, value):
    """
    Public endpoint that gets earthquake by term
    """
    terms = {key: value}
    earthquake = Earthquake(db_conn())
    result = earthquake.search(terms)
    return app.response_class(
        response=json.dumps(list(result)),
        status=200,
        mimetype='application/json'
    )

@app.route('/earthquake/create', methods=['POST'])
def create_earthquake():
    """
    Public endpoint that creates a new earthquake
    """
    data = request.get_json(force=True, silent=True)
    if data is None:
        err_msg = 'Error Parsing json {}'.format(request.get_data(as_text=True))
        return app.response_class(
            response=json.dumps({'message': err_msg}),
            status=500,
            mimetype='application/json'
        )
    earthquake = Earthquake(db_conn())
    try:
        result = earthquake.create(data)
        print('Inserted id: {}'.format(result.inserted_id))
        data['_id'] = str(data.get('_id'))
        return app.response_class(
            response=json.dumps(data),
            status=201,
            mimetype='application/json'
        )
    except Exception as e:
        return app.response_class(
            response=json.dumps({'message': str(e)}),
            status=500,
            mimetype='application/json'
        )
